!include <win32.mak>

all: kirillroute.dll TestDll.exe

.cpp.obj:
  $(cc) $(cdebug) $(cflags) $(cvars) -DUNICODE -D_UNICODE $*.cpp

kirillroute.dll: kirillroute.obj
    $(link) $(linkdebug) $(dlllflags) \
      $** -out:kirillroute.dll -def:kirillroute.def   \
      $(conlibsdll) user32.lib shlwapi.lib

TestDll.exe: testdll.obj
    $(link) $(linkdebug) $(lflags) \
      $** -out:TestDll.exe   \
      $(conlibs) shlwapi.lib mprapi.Lib user32.lib


clean:
	-del *.obj
        -del *.pdb
        -del *.lib
        -del *.exp
       

cleanall:clean
	-del *.exe
	-del *.dll
