// TestDll.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include <windows.h>
#include <strsafe.h>

typedef DWORD (__cdecl *MYPROC)(); 

void ErrorExit(LPTSTR lpszFunction) 
{ 
    // Retrieve the system error message for the last-error code

    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError(); 

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );

    // Display the error message and exit the process

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
        (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR)); 
    StringCchPrintf((LPTSTR)lpDisplayBuf, 
        LocalSize(lpDisplayBuf) / sizeof(TCHAR),
        TEXT("%s failed with error %d: %s"), 
        lpszFunction, dw, lpMsgBuf); 
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK); 

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    ExitProcess(dw); 
}

int main(int /*argc*/, char* /*argv*/[])
{
    MYPROC ProcAdd; 
    HINSTANCE hinstLib = NULL;

    hinstLib = LoadLibrary(TEXT("kirillroute.dll")); 	
    if (hinstLib != NULL) 
    {
		ProcAdd = (MYPROC) GetProcAddress(hinstLib, "MprAdminInitializeDll"); 
 
        // If the function address is valid, call the function. 
        if (NULL != ProcAdd) 
        {
            (ProcAdd) (); 
			MessageBox(NULL, TEXT("Call successfull."), TEXT("Ok"), MB_OK); 
        }
		else
			MessageBox(NULL, TEXT("Load successfull. Call fail."), TEXT("Ok"), MB_OK); 

        // Free the DLL module.
		FreeLibrary(hinstLib); 
    }
	else
	{
		ErrorExit(TEXT("Load UserRestrict"));
	}
	
	return 0;
}

