
#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <stdio.h>

#include <mprapi.h>

BOOL DllMain(
    HINSTANCE /*hinstDLL*/,  // handle to DLL module
    DWORD fdwReason,     // reason for calling function
    LPVOID /*lpReserved*/ )  // reserved
{
    // Perform actions based on the reason for calling.
    switch( fdwReason )
    {
       case DLL_PROCESS_ATTACH:
       // Initialize once for each new process.
       // Return FALSE to fail DLL load.
        break;

       case DLL_THREAD_ATTACH:
       // Do thread-specific initialization.
       break;


       case DLL_THREAD_DETACH:
       // Do thread-specific cleanup.
       break;

       case DLL_PROCESS_DETACH:
       // Perform any necessary cleanup.
       break;

    }

    return TRUE;  // Successful DLL_PROCESS_ATTACH.
}

//
// Function used for DLL specific initialization.
//
// Returns: NO_ERROR if successful
//		    The last error number if an error is encountered
//
DWORD WINAPI
MprAdminInitializeDll(void)
{
    DWORD dwRetVal = ERROR_SUCCESS;
    return dwRetVal;
}

//
// Function used to form DLL specific cleanup.
//
DWORD WINAPI
MprAdminTerminateDll(void)
{
    return NO_ERROR;
}

//
// Function is called whenever a link is disconnected.
//
// Parameters: pointer to a RAS_PORT_0 structure
//	      pointer to a RAS_PORT_1 structure
//
VOID WINAPI
MprAdminLinkHangupNotification(RAS_PORT_0 * /*pRasPort0*/,
                               RAS_PORT_1 * /*pRasPort1*/)
{
}

//
// Function is called whenever a new link is made.
//
// Parameters: pointer to a RAS_PORT_0 structure
//			   pointer to a RAS_PORT_1 structure
//
BOOL WINAPI
MprAdminAcceptNewLink(RAS_PORT_0 * /*pRasPort0*/,
                      RAS_PORT_1 * /*pRasPort1*/)
{
    return TRUE;
}

//
// Function is called whenever a call is disconnected.
//
// Parameters: pointer to a RAS_CONNECTION_0 structure
//	      pointer to a RAS_CONNECTION_1 structure
//	      pointer to a RAS_CONNECTION_2 structure
//
VOID WINAPI
MprAdminConnectionHangupNotification2(RAS_CONNECTION_0 * /*pRasConnection0*/,
                                      RAS_CONNECTION_1 * /*pRasConnection1*/,
                                      RAS_CONNECTION_2 * /*pRasConnection2*/)
{
}

//
// Function is called whenever a call is connected.
//
// Parameters: pointer to a RAS_CONNECTION_0 structure
//			   pointer to a RAS_CONNECTION_1 structure
//			   pointer to a RAS_CONNECTION_2 structure
//
BOOL WINAPI
MprAdminAcceptNewConnection2(RAS_CONNECTION_0 * /*pRasConnection0*/,
                             RAS_CONNECTION_1 * /*pRasConnection1*/,
                             RAS_CONNECTION_2 * pRasConnection2)
{
    if (NULL == pRasConnection2)
        return TRUE;

	wchar_t* UserName		= pRasConnection2->wszUserName;
    wchar_t* RemoteAddress	= pRasConnection2->PppInfo2.ip.wszRemoteAddress;

    int len = (int)wcslen(UserName);
    if (len > 6)	// office20@
    {
		wchar_t buf[120];
        wcsncpy(buf, UserName, 6);
        buf[6] = 0;

        if (wcscmp(buf, TEXT("office")) == 0)
        {
            wchar_t* net_dec = &buf[0];
            UserName += 6;
            do {
                *net_dec++ = *UserName++;
            }
            while (*UserName != 0 && *UserName != L'@');
            *net_dec = 0;
            int subnet = _wtoi(buf);

            if (subnet > 0 && subnet < 255)
            {
                swprintf(buf, 120, L"route -p change 192.168.%d.0/24 %s", subnet, RemoteAddress);
                _wsystem(buf);
            }
        }
    }
    return TRUE;
}
